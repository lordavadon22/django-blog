"""infinity_crm URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path

from blog import views

urlpatterns = [
    path('', views.BlogListView.as_view(), name='home'),
    path('category/<str:slug>/', views.CategoryView.as_view(), name='category'),
    path('post/<str:slug>/', views.PostView.as_view(), name='post'),
    path('tag/<str:slug>/', views.PostsByTag.as_view(), name='tag'),
    path('search/', views.SearchView.as_view(), name='search'),
]
