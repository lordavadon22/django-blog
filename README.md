## Django blog

Для разворачивания данного проекта у себя на сервере нужно:

- Сервер Ubuntu linux 18.04 и выше
- docker версии 20 и выше
- docker-compose версии 2 и выше

## Процедура установки проста:

#### 1) Выполнить клонирование репозитория на сервер

    git clone https://gitlab.com/lordavadon22/django-blog

#### 2) В корне проекта создать env-dev

    DJANGO_SECRET_KEY=django-insecure-tbfoq7x#2k8@&d6d34^orc5c6o+#i)mgepqn2ke7eqiq@6xi_k
    DB_NAME=blog
    DB_USER=blog
    DB_PASSWORD=blog
    DB_HOST=db
    DB_PORT=5432

#### 3) В корне проекта создать env-dev_db

    POSTGRES_DB=blog
    POSTGRES_USER=blog
    POSTGRES_PASSWORD=blog

#### 4) Запустить контейнеры

    docker-compose up -d --build

#### 5) Выполнить миграцию базы данных, создать супер пользователя и собрать статику на сервере

    docker-compose exec blog_django python manage.py migrate
    docker-compose exec blog_django python manage.py createsuperuser
    docker-compose exec blog_django python manage.py collectstatic
    
#### 6) Проверить состояние контейнеров

    docker-compose logs -f

#### 7) Можно переходить на сайт по адресу

    http(s)://<ip_адрес_сервера>/
